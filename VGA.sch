EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "12-bit RGB color for VGA module / 4-bit color depth"
Date "2021-03-13"
Rev "1.2"
Comp "Ulices AH"
Comment1 "4 bits per color channel/sample (bps)"
Comment2 "2⁴=16 different red, green and blue tonalities"
Comment3 "12 bits per pixel (bpp)"
Comment4 "2¹²=4096 different colors"
$EndDescr
$Comp
L Connector:DB15_Female_HighDensity J2
U 1 1 5CB520F0
P 6575 4025
F 0 "J2" H 6575 4892 50  0000 C CNN
F 1 "DB15_Female_HighDensity" H 6575 4801 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-15-HD_Female_Horizontal_P2.29x2.54mm_EdgePinOffset9.40mm" H 5625 4425 50  0001 C CNN
F 3 " ~" H 5625 4425 50  0001 C CNN
	1    6575 4025
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3225 5150 3225
Wire Wire Line
	4850 3325 3900 3325
Wire Wire Line
	3900 3425 4550 3425
Wire Wire Line
	4250 3525 3900 3525
$Comp
L Device:R R5
U 1 1 5CB5D8A7
P 5300 3625
F 0 "R5" V 5200 3625 50  0000 C CNN
F 1 "510" V 5300 3625 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5230 3625 50  0001 C CNN
F 3 "~" H 5300 3625 50  0001 C CNN
	1    5300 3625
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5CB5D8AD
P 5000 3725
F 0 "R6" V 4900 3725 50  0000 C CNN
F 1 "1k" V 5000 3725 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 3725 50  0001 C CNN
F 3 "~" H 5000 3725 50  0001 C CNN
	1    5000 3725
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5CB5D8B3
P 4700 3825
F 0 "R7" V 4600 3825 50  0000 C CNN
F 1 "2k" V 4700 3825 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4630 3825 50  0001 C CNN
F 3 "~" H 4700 3825 50  0001 C CNN
	1    4700 3825
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5CB5D8B9
P 4400 3925
F 0 "R8" V 4300 3925 50  0000 C CNN
F 1 "4k" V 4400 3925 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4330 3925 50  0001 C CNN
F 3 "~" H 4400 3925 50  0001 C CNN
	1    4400 3925
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 3625 5150 3625
Wire Wire Line
	4850 3725 3900 3725
Wire Wire Line
	3900 3825 4550 3825
Wire Wire Line
	4250 3925 3900 3925
$Comp
L Device:R R9
U 1 1 5CB5E39F
P 5300 4025
F 0 "R9" V 5200 4025 50  0000 C CNN
F 1 "510" V 5300 4025 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5230 4025 50  0001 C CNN
F 3 "~" H 5300 4025 50  0001 C CNN
	1    5300 4025
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5CB5E3A5
P 5000 4125
F 0 "R10" V 4900 4125 50  0000 C CNN
F 1 "1k" V 5000 4125 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 4125 50  0001 C CNN
F 3 "~" H 5000 4125 50  0001 C CNN
	1    5000 4125
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5CB5E3AB
P 4700 4225
F 0 "R11" V 4600 4225 50  0000 C CNN
F 1 "2k" V 4700 4225 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4630 4225 50  0001 C CNN
F 3 "~" H 4700 4225 50  0001 C CNN
	1    4700 4225
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5CB5E3B1
P 4400 4325
F 0 "R12" V 4300 4325 50  0000 C CNN
F 1 "4k" V 4400 4325 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4330 4325 50  0001 C CNN
F 3 "~" H 4400 4325 50  0001 C CNN
	1    4400 4325
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 4025 5150 4025
Wire Wire Line
	4850 4125 3900 4125
Wire Wire Line
	3900 4225 4550 4225
Wire Wire Line
	4250 4325 3900 4325
$Comp
L Device:R R13
U 1 1 5CB6013B
P 5300 4425
F 0 "R13" V 5200 4425 50  0000 C CNN
F 1 "82" V 5300 4425 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5230 4425 50  0001 C CNN
F 3 "~" H 5300 4425 50  0001 C CNN
	1    5300 4425
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5CB60141
P 5000 4525
F 0 "R14" V 4900 4525 50  0000 C CNN
F 1 "82" V 5000 4525 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 4525 50  0001 C CNN
F 3 "~" H 5000 4525 50  0001 C CNN
	1    5000 4525
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 3225 5550 3225
$Comp
L power:GND #PWR01
U 1 1 5CB63FE5
P 6025 4575
F 0 "#PWR01" H 6025 4325 50  0001 C CNN
F 1 "GND" H 6030 4402 50  0000 C CNN
F 2 "" H 6025 4575 50  0001 C CNN
F 3 "" H 6025 4575 50  0001 C CNN
	1    6025 4575
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 3325 5550 3325
Wire Wire Line
	4850 3425 5550 3425
Wire Wire Line
	4550 3525 5550 3525
Wire Wire Line
	5150 3725 5550 3725
Wire Wire Line
	4850 3825 5550 3825
Wire Wire Line
	4550 3925 5550 3925
Wire Wire Line
	4550 4325 5550 4325
Wire Wire Line
	4850 4225 5550 4225
Wire Wire Line
	5150 4125 5550 4125
Wire Wire Line
	3900 4525 4850 4525
Wire Wire Line
	3900 4425 5150 4425
Text Label 5550 3225 0    50   ~ 0
R
Text Label 5550 3625 0    50   ~ 0
G
Text Label 5550 4025 0    50   ~ 0
B
Wire Wire Line
	6275 4425 6025 4425
Wire Wire Line
	6025 4425 6025 4575
Wire Wire Line
	6275 3525 6025 3525
Wire Wire Line
	6025 3525 6025 3725
Connection ~ 6025 4425
Wire Wire Line
	6275 3725 6025 3725
Connection ~ 6025 3725
Wire Wire Line
	6025 3725 6025 3925
Wire Wire Line
	6275 3925 6025 3925
Connection ~ 6025 3925
Wire Wire Line
	6025 3925 6025 4325
Wire Wire Line
	6275 4325 6025 4325
Connection ~ 6025 4325
Wire Wire Line
	6025 4325 6025 4425
NoConn ~ 6275 4125
NoConn ~ 6275 4225
NoConn ~ 6875 3625
NoConn ~ 6875 3825
NoConn ~ 6875 4425
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5DC5576F
P 4450 4725
F 0 "#FLG01" H 4450 4800 50  0001 C CNN
F 1 "PWR_FLAG" H 4450 4898 50  0000 C CNN
F 2 "" H 4450 4725 50  0001 C CNN
F 3 "~" H 4450 4725 50  0001 C CNN
	1    4450 4725
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x16 J1
U 1 1 5DE9EABD
P 3700 3925
F 0 "J1" H 3618 2900 50  0000 C CNN
F 1 "Conn_01x16" H 3618 2991 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 3700 3925 50  0001 C CNN
F 3 "~" H 3700 3925 50  0001 C CNN
	1    3700 3925
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5DEA2BE8
P 4050 4725
F 0 "#PWR02" H 4050 4475 50  0001 C CNN
F 1 "GND" H 4055 4552 50  0000 C CNN
F 2 "" H 4050 4725 50  0001 C CNN
F 3 "" H 4050 4725 50  0001 C CNN
	1    4050 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4625 4050 4625
Wire Wire Line
	4050 4625 4050 4725
Wire Wire Line
	4450 4725 4450 4625
Wire Wire Line
	4450 4625 4050 4625
Connection ~ 4050 4625
$Comp
L Device:R R1
U 1 1 5CB52963
P 5300 3225
F 0 "R1" V 5200 3225 50  0000 C CNN
F 1 "510" V 5300 3225 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5230 3225 50  0001 C CNN
F 3 "~" H 5300 3225 50  0001 C CNN
	1    5300 3225
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5CB54C9F
P 5000 3325
F 0 "R2" V 4900 3325 50  0000 C CNN
F 1 "1k" V 5000 3325 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 3325 50  0001 C CNN
F 3 "~" H 5000 3325 50  0001 C CNN
	1    5000 3325
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5CB55603
P 4700 3425
F 0 "R3" V 4600 3425 50  0000 C CNN
F 1 "2k" V 4700 3425 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4630 3425 50  0001 C CNN
F 3 "~" H 4700 3425 50  0001 C CNN
	1    4700 3425
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5CB55609
P 4400 3525
F 0 "R4" V 4300 3525 50  0000 C CNN
F 1 "4k" V 4400 3525 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4330 3525 50  0001 C CNN
F 3 "~" H 4400 3525 50  0001 C CNN
	1    4400 3525
	0    1    1    0   
$EndComp
Text Label 5550 4425 0    50   ~ 0
H
Text Label 5550 4525 0    50   ~ 0
V
Wire Wire Line
	6975 4225 6875 4225
Wire Wire Line
	5550 4225 5550 4325
Connection ~ 5550 4225
Wire Wire Line
	5550 4025 5550 4125
Wire Wire Line
	5550 4025 5450 4025
Connection ~ 5550 4125
Wire Wire Line
	5550 4125 5550 4225
Wire Wire Line
	5550 3625 5450 3625
Wire Wire Line
	5550 3625 5550 3725
Connection ~ 5550 3725
Wire Wire Line
	5550 3725 5550 3825
Connection ~ 5550 3825
Wire Wire Line
	5550 3525 5550 3425
Connection ~ 5550 3425
Wire Wire Line
	5550 3425 5550 3325
Connection ~ 5550 3325
Wire Wire Line
	5550 3325 5550 3225
$Comp
L power:GND #PWR0101
U 1 1 5DF6A5E2
P 4150 3125
F 0 "#PWR0101" H 4150 2875 50  0001 C CNN
F 1 "GND" H 4155 2952 50  0000 C CNN
F 2 "" H 4150 3125 50  0001 C CNN
F 3 "" H 4150 3125 50  0001 C CNN
	1    4150 3125
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 3125 3900 3125
Text Notes 6000 2925 0    50   ~ 0
DE-15F recommended connnector\nfor the VGA board
Text Notes 4075 2750 0    50   ~ 0
16-pin SIL header connector for\nRGB and Hsync/Vsync signals
Wire Wire Line
	5550 3825 6275 3825
Wire Wire Line
	5550 3825 5550 3925
Wire Wire Line
	5550 4025 6275 4025
Connection ~ 5550 4025
Wire Wire Line
	5875 3625 5875 3525
Wire Wire Line
	5875 3525 5550 3525
Wire Wire Line
	5875 3625 6275 3625
Connection ~ 5550 3525
Wire Wire Line
	5625 4525 5625 4925
Wire Wire Line
	5625 4925 6975 4925
Wire Wire Line
	6975 4925 6975 4225
Wire Wire Line
	5150 4525 5625 4525
Wire Wire Line
	5700 4850 7050 4850
Wire Wire Line
	7050 4850 7050 4025
Wire Wire Line
	6875 4025 7050 4025
Wire Wire Line
	5450 4425 5700 4425
Wire Wire Line
	5700 4425 5700 4850
$Comp
L Mechanical:MountingHole H1
U 1 1 60589DCB
P 7525 3775
F 0 "H1" H 7625 3821 50  0000 L CNN
F 1 "MountingHole" H 7625 3730 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad" H 7525 3775 50  0001 C CNN
F 3 "~" H 7525 3775 50  0001 C CNN
	1    7525 3775
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 6058A15E
P 7525 4025
F 0 "H2" H 7625 4071 50  0000 L CNN
F 1 "MountingHole" H 7625 3980 50  0000 L CNN
F 2 "MountingHole:MountingHole_3mm_Pad" H 7525 4025 50  0001 C CNN
F 3 "~" H 7525 4025 50  0001 C CNN
	1    7525 4025
	1    0    0    -1  
$EndComp
Text Notes 7350 3550 0    50   ~ 0
Connector holes 3[mm]
$EndSCHEMATC
